﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ECommercePortal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ECommercePortal.Controllers
{
   /* [Route("api/[controller]")]
    [ApiController]*/
    public class UserController : Controller
    {
        Uri baseaddress = new Uri("http://localhost:50900/api");
        HttpClient client;
        public UserController()
        {
            client = new HttpClient();
            client.BaseAddress = baseaddress;
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Auth(User user)
        {
            string data = JsonConvert.SerializeObject(user);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response;
            try
            {
                response = client.PostAsync(client.BaseAddress + "/Users/Authenticate", content).Result;
            }
            catch
            {
                return RedirectToAction("Error");
            }
            if (response.IsSuccessStatusCode)
            {
                string token = response.Content.ReadAsStringAsync().Result;

                if (token != null)
                {
                    TokenInfo.StringToken = token;
                    TokenInfo.Username = user.Username;
                    //TokenInfo.UserID = user.UserID;
                    User user1 = new User();
                    HttpResponseMessage response1 = client.GetAsync(client.BaseAddress + "/users/GetUserDetails?username=" + user.Username).Result;
                    string result = response1.Content.ReadAsStringAsync().Result;
                    user1 = JsonConvert.DeserializeObject<User>(result);
                    TokenInfo.Username = user1.Username;                   
                    TokenInfo.UserID = user1.UserID;
                    return RedirectToAction("Index", "Customer");
                    //TokenInfo.username = user.username;
                   // return RedirectToAction("Detail", "User",user.UserID);
                    //return RedirectToAction("Index", "Customer");
                }
                else
                {
                    return Unauthorized();
                }
            }
            /*TokenInfo.StringToken = "";
            TokenInfo.UserID = 0;
            TokenInfo.username = "";*/

            return RedirectToAction("Login");

        }
        public IActionResult Logout()
        {
            TokenInfo.StringToken = null;
            TokenInfo.UserID = 0;
            TokenInfo.Username = null;

            return RedirectToAction("Login");
        }
        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetUserDetails(string username)
        {
            User user = new User();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Users/GetUserDetails?username=" +username).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            user = JsonConvert.DeserializeObject<User>(result);
            TokenInfo.Username = user.Username;
            TokenInfo.UserID = user.UserID;
            return RedirectToAction("Index", "Customer");
        }
    }
}
