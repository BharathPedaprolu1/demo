﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
namespace ECommercePortal.Models
{
    public class Register
    {
        [Key]
         public int userid { get; set; }
        [Required]
        public string Username { get; set; }
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string password { get; set; }
       
        
    }
}
