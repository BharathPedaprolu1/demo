﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendorMicroservice.Models;

namespace VendorMicroservice.Helper
{
    public class Data
    {
        public static List<Vendor> vendors = new List<Vendor>
        {
            new Vendor{VendorId = 111,VendorName="mouli",DeliveryCharge=2150,Rating=4,ExpectedDateOfDelivery=7},
            new Vendor{VendorId = 112,VendorName="susheel",DeliveryCharge=2650,Rating=5,ExpectedDateOfDelivery=15},
            new Vendor{VendorId = 113,VendorName="deepthi",DeliveryCharge=2950,Rating=4,ExpectedDateOfDelivery=8},
            new Vendor{VendorId = 114,VendorName="anima",DeliveryCharge=1120,Rating=5,ExpectedDateOfDelivery=6},
            new Vendor{VendorId = 115,VendorName="nikita",DeliveryCharge=1990,Rating=3,ExpectedDateOfDelivery=9}
        };

        public static List<VendorStock> stocks = new List<VendorStock>
        {
            new VendorStock{ProductId = 1,VendorId = 111,StockInHand=50,ExpectedStockReplenishmentDate="2021-04-19"},
            new VendorStock{ProductId = 2,VendorId = 111,StockInHand=150,ExpectedStockReplenishmentDate="2021-05-01"},
            new VendorStock{ProductId = 1,VendorId = 112,StockInHand=76,ExpectedStockReplenishmentDate="2021-05-01"},
            new VendorStock{ProductId = 2,VendorId = 112,StockInHand=0,ExpectedStockReplenishmentDate="2021-04-19"},
            new VendorStock{ProductId = 3,VendorId = 113,StockInHand=0,ExpectedStockReplenishmentDate="2021-04-20"},
            new VendorStock{ProductId = 1,VendorId = 113,StockInHand=202,ExpectedStockReplenishmentDate="2021-05-25"},
            new VendorStock{ProductId = 3,VendorId = 114,StockInHand=0,ExpectedStockReplenishmentDate="2021-04-27"},
            new VendorStock{ProductId = 4,VendorId = 114,StockInHand=85,ExpectedStockReplenishmentDate="2021-05-20"},
            new VendorStock{ProductId = 2,VendorId = 115,StockInHand=185,ExpectedStockReplenishmentDate="2021-12-03"},
            new VendorStock{ProductId = 5,VendorId = 115,StockInHand=150,ExpectedStockReplenishmentDate="2021-12-08"}
        };
    }
}
