using Authorization.Repository;
using Authorization.Controllers;
using AuthorizationMicroservice.Models;
using AuthorizationMicroservice.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuthorizationTesting
{
    public class Tests
    {
        List<UserModel> user;

        [SetUp]
        public void Setup()
        {
            
        }

    [Test]
        public void IsTokenNotNull_When_UserCredentialsAreValid()
        {
            Mock<IUserRepository> config = new Mock<IUserRepository>();
            UsersController obj = new UsersController(config.Object);

            var res = obj.Authenticate("mouli","123");

            Assert.IsNotNull(res);
        }


        [SetUp]
        public void Setup1()
        {
            user = new List<UserModel>()
            {
                new UserModel{ userid=1,password="123",username="mouli"},
                new UserModel{ userid=2,password="123",username="susheel"},
                new UserModel{ userid=3,password="123",username="anima"}
            };
        }

        [Test]
        public void GetUserDetails_ValidInput_ReturnsOkRequest()
        {
           string Username ="mouli";

            var mock = new Mock<IUserRepository>();

            mock.Setup(x => x.UserDetails(Username)).Returns((user.Where(x => x.username == Username)).FirstOrDefault());

            UsersController obj = new UsersController(mock.Object);

            var data = obj.GetUserDetails(Username);

            var res = data as ObjectResult;

            Assert.AreEqual(200, res.StatusCode);
        }
      
        [Test]
        public void GetUserDetails_InvalidInput_ReturnsNotFoundResult()
        {
            string Username = "vikas";

            var mock = new Mock<IUserRepository>();

            mock.Setup(x => x.UserDetails(Username)).Returns((user.Where(x => x.username == Username)).FirstOrDefault());

            UsersController obj = new UsersController(mock.Object);

            var data = obj.GetUserDetails(Username);

            var res = data as NotFoundResult;
            
            Assert.AreEqual(404, res.StatusCode);        
        }
    }
}