﻿namespace AuthorizationMicroservice.Repository
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}