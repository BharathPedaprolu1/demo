﻿using AuthorizationMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.Repository
{
    public interface IUserRepository
    {
        public bool isUniqueUser(string username);
        public UserModel Authenticate(string username, string password);
        public UserModel UserDetails(string username);
        public Register RegisterDetails(int userid, string username, string Email, string password);
    }
}
