﻿using AuthorizationMicroservice.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

using System.Threading.Tasks;

namespace Authorization.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly AppSettings _appSettings;

        private readonly string Key;
        public UserRepository(string Key)
        {
            this.Key = Key;
        }
        
        public List<UserModel> userlist = new List<UserModel>() {
           new UserModel{ userid=1,password="123",username="bharath"},
            new UserModel{ userid=2,password="123",username="susheel"},
            new UserModel{ userid=3,password="123",username="anima"},
            new UserModel{ userid=4,password="123",username="nikita"},
            new UserModel{ userid=5,password="123",username="deepthi"}
        };
       
        public UserRepository(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        Register IUserRepository.RegisterDetails(int userid, string username, string Email, string password)
        {

            return true;
        }

        UserModel IUserRepository.Authenticate(string UserName, string password)
        {
            var user = userlist.SingleOrDefault(x => x.username == UserName && x.password == password);
            if (user == null)
            {
                return null;
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var k = "This is my jwt authentication demo";
            var key = Encoding.ASCII.GetBytes(k);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.UserData, user.username)
                }),
               /* notBefore: DateTime.UtcNow,*/
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.password = "";
            return user;
        }

        bool IUserRepository.isUniqueUser(string UserName)
        {
            var user = userlist.SingleOrDefault(x => x.username == UserName);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        public UserModel UserDetails(string username)
        {

            foreach (var user in userlist)
            {
                if (user.username == username)
                {
                    return user;
                }
            }
            return null;

        }
    }
}